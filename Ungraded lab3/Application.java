public class Application{
	
	public static void main(String[] args){
		
		Panda pan1 = new Panda();
		pan1.height = 200.54;
		pan1.age = 18;
		pan1.food = "bamboo";
		
		Panda pan2 = new Panda();
		pan2.height = 314.86;
		pan2.age = 20;
		pan2.food = "veggies";
		
		System.out.println(pan1.height);
		System.out.println(pan1.age);
		System.out.println(pan1.food);
		System.out.println(pan2.height);
		System.out.println(pan2.age);
		System.out.println(pan2.food);
		
		pan1.jump(pan1.height);
		pan1.eat(pan1.food);
		pan2.jump(pan2.height);
		pan2.eat(pan2.food);
		
		Panda[] embarrassment = new Panda[3];
		embarrassment[0] = pan1;
		embarrassment[1] = pan2;
		System.out.println(embarrassment[0].height);
		
		embarrassment[2] = new Panda();
		embarrassment[2].height = 483.29;
		System.out.println(embarrassment[2].height);
	}
}