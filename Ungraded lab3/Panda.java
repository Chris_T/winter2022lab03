public class Panda{
	
	public double height;
	public int age;
	public String food;

	public void jump(double height){
	
		System.out.println("I just jumped " + height + " inches!");
	}
	
	public void eat(String food){
		
		System.out.println("I just ate " + food);
	}
}