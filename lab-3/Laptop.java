public class Laptop{
	
	public String make;
	public String processor;
	public int ram;
	
	public void fullDetails(String make, String processor, int ram){
		
		System.out.println("The " + make + " laptop has " + ram + " GB of RAM and " + processor + " as processor");
	}
}