import java.util.Scanner;

public class Shop{
	
	public static void main (String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		Laptop[] pc = new Laptop[4];
		
		for(int i = 0; i < pc.length; i++){
			
			pc[i] = new Laptop();
			System.out.println("Enter computer #" + (i+1) + "'s make");
			pc[i].make = scan.nextLine();
			System.out.println("Enter computer #" + (i+1) + "'s processor");
			pc[i].processor = scan.nextLine();
			System.out.println("Enter computer #" + (i+1) + "'s RAM");
			pc[i].ram = scan.nextInt();
			scan.nextLine();
		}
		
		System.out.println(pc[3].make);
		System.out.println(pc[3].processor);
		System.out.println(pc[3].ram);
		
		pc[3].fullDetails(pc[3].make, pc[3].processor, pc[3].ram);
	}
}